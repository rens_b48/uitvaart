$(function(){
    $('form').submit(function(){
        var errors = false;
        $('.error_message').remove();
        $('*[data-validate]').each(function(){
            var field = $(this);
            var conditions = field.data('validate');
            field.removeClass('input_error');
            if (conditions.indexOf("*") >= 0 && field.val().length < 3) {
                field.addClass('input_error');
                $( "<span class='error_message'>Dit veld is verplicht</span>" ).insertAfter( field );
                errors = true;
            }
            if (conditions.indexOf("varchar:255") >= 0 && field.val().length > 255) {
                field.addClass('input_error');
                $( "<span class='error_message'>De tekst is te lang</span>" ).insertAfter( field );
                errors = true;
            }
        });
        if(errors) {
            $("html, body").animate({ scrollTop: 0 }, "slow");
            return false;
        }
    });
});
