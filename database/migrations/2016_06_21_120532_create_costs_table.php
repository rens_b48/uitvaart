<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costs', function (Blueprint $table) {

            $table->increments('deceased_id');
            $table->string('recordings_and_output_costs');
            $table->string('final_care_costs');
            $table->string('car_costs');
            $table->string('phone_costs');
            $table->string('carrier_costs');
            $table->string('deductions1');
            $table->string('onk_carriers');
            $table->string('grave_digging_costs');
            $table->string('deductions2');
            $table->string('rent_graves');
            $table->string('rent_bier');
            $table->string('sink_device_costs');
            $table->string('bell_ringing_costs');
            $table->string('deductions3');
            $table->string('tip');
            $table->string('coffin_costs');
            $table->string('mourning_car_costs');
            $table->string('cooling_costs');
            $table->string('good_grief_costs');
            $table->string('carry_bier_costs');
            $table->string('cross_costs');
            $table->string('sticker_and_doc_costs');
            $table->string('following_car_costs');
            $table->string('condolances_book_costs');
            $table->string('death_certificate_costs');
            $table->string('cd_dvd_recording_costs');
            $table->string('lying_in_state_costs');
            $table->string('hospital_costs');
            $table->string('printed_letter_costs');
            $table->string('postage_costs');
            $table->string('liturgies_costs');
            $table->string('sendset_costs');
            $table->string('ad_LC_costs');
            $table->string('ad_FD_costs');
            $table->string('sneeker_niews_ad_costs');
            $table->string('other_paper_costs');
            $table->string('thanks_card_costs');
            $table->string('ad_costs');
            $table->string('prayer_cards_costs');
            $table->string('faxing_costs');
            $table->string('sound_inst');
            $table->string('good_grief_rent');
            $table->string('bed_cooler_costs');
            $table->string('airco_costs');
            $table->string('nameplate');
            $table->string('rent_church_room');
            $table->string('sacristan_costs');
            $table->string('onk_pred_costs');
            $table->string('organisation_costs');
            $table->string('speaker_costs');
            $table->string('cremation_costs');
            $table->string('ashes_completion_costs');
            $table->string('after_visitors_costs');
            $table->string('flower_costs');
            $table->string('particularities_costs');
            $table->string('overlook_costs');
            $table->string('administration_costs');

            $table->string('insurance_policy_1');
            $table->string('payment_1');
            $table->string('insurance_policy_2');
            $table->string('payment_2');
            $table->string('insurance_policy_3');
            $table->string('payment_3');
            $table->string('insurance_policy_4');
            $table->string('payment_4');

            $table->string('total');
            $table->string('ver_discount');
            $table->string('payment_discount');
            $table->string('to_pay');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
