<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUndertakerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('undertaker', function (Blueprint $table) {
            
            $table->string('undertaker_name');
            $table->string('undertaker_address');
            $table->string('undertaker_zipcode');
            $table->string('undertaker_city');
            $table->string('undertaker_phone');
            $table->string('funeral_director');
            $table->string('undertaker_email');
            $table->date('undertaker_sign_date');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
