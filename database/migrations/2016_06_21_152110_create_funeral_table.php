<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funeral', function (Blueprint $table) {
            $table->increments('deceased_id');
            $table->string('date_funeral');
            $table->string('condolences_moment_1_date');
            $table->string('condolences_moment_1_time');
            $table->string('condolences_moment_1_location');
            $table->string('condolences_moment_2_date');
            $table->string('condolences_moment_2_time');
            $table->string('condolences_moment_2_location');
            $table->string('memorial_service_location');
            $table->string('memorial_service_time');
            $table->string('funeral_location');
            $table->string('funeral_time');
            $table->string('cremation_location');
            $table->string('cremation_time');
            $table->string('ashes_destination');

            $table->string('cremation_number');

            $table->string('predecessor');
            $table->string('organist');

            $table->string('mourning_car');
            $table->string('support_car');
            $table->string('support_car_count');
            $table->string('family_collected');

            $table->string('carrier_1');
            $table->string('carrier_2');
            $table->string('carrier_3');
            $table->string('carrier_4');
            $table->string('carrier_5');
            $table->string('carrier_6');
            $table->string('carrier_7');
            $table->string('carrier_8');

            $table->string('descend_coffin');
            $table->string('new_graves');
            $table->string('new_graves_nr');
            $table->string('old_graves');
            $table->string('old_graves_no');
            $table->string('user');
            $table->string('grave_digging_by');
            $table->string('stone_pickup');
            $table->string('family_vault');
            $table->string('bell_ringing');
            $table->string('bell_ringing_by');
            $table->string('driving_bier');

            $table->string('flowerist');
            $table->string('flower1_number');
            $table->string('flower1_text');
            $table->string('flower2_number');
            $table->string('flower2_text');

            $table->string('after_visitors');
            $table->string('visitors_count');
            $table->string('consumptions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
