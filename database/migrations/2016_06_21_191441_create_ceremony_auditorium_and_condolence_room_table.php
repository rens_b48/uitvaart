<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCeremonyAuditoriumAndCondolenceRoomTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ceremony_auditorium', function (Blueprint $table) {
            
            $table->increments('deceased_id');
            $table->string('expected_indiviuals');
            $table->string('extra_time_auditorium');
            $table->string('auditorium_extra_time_after_consulation_with_crematorium');
            $table->string('to_keep_from_eyes');
            $table->string('when_to_keep_from_eyes');
            $table->string('liturgical_attributes_at_catafalque');
            $table->string('music_type');
            $table->string('amount_cassettes');
            $table->string('amount_cds');
            $table->string('amount_DVDs');
            $table->string('use_condolences_room');
            $table->string('coffee_room_extra_time');
            $table->string('coffee_room_extra_time_after_consulation_with_crematorium');
            $table->string('consumption_types');
            $table->string('consumptions_amount');
            $table->string('consumption_wishes');
            $table->string('additional_wishes_flowers');
            $table->string('arrival_family_procession');
            $table->string('remarks');
            $table->string('ashes_destination_type');
            $table->string('speakers');
            $table->string('speaker_amount');
            $table->string('other_flower_wish');
            $table->string('music_1');
            $table->string('music_2');
            $table->string('music_3');
            $table->string('music_4');
            $table->string('music_5');
            $table->string('present_video');
            $table->string('present_DVD');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
