<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general', function (Blueprint $table) {
            $table->increments('deceased_id');
            $table->string('funeral_association');
            $table->string('total_number');
            $table->string('serial_number');
            $table->string('name_declarant');
            $table->string('firstname_declarant');
            $table->string('address_declarant');
            $table->string('city_declarant');
            $table->date('date_of_birth');
            $table->string('age_declarant');
            $table->string('city_of_birth');
            $table->string('maritual_status');
            $table->string('father');
            $table->string('father_dead');
            $table->string('mother');
            $table->string('mother_dead');
            $table->string('adult_heir_children');
            $table->string('minor_heir_children');
            $table->date('date_of_death');
            $table->string('hour_of_death');
            $table->string('city_of_death');
            $table->string('doctor');
            $table->string('telephone');

            $table->string('denomination');
            $table->string('member_yarden');
            $table->string('yarden_member_number');

            $table->string('lying_in_state_auditorium');
            $table->date('lying_in_state_start');
            $table->date('lying_in_state_end');
            $table->string('final_care');
            $table->string('coffin_number');
            $table->string('coffin_length');
            $table->string('coffin_carriable');
            $table->string('coffin_slipcover');
            $table->string('cross');
            $table->string('coffin_cool_lid');
            $table->string('coffin_heatsink');
            $table->string('coffin_airco');

            $table->string('press-work');
            $table->string('presswork-amount');
            $table->string('postage-amount');
            $table->string('eu');
            $table->string('w');
            $table->string('printer');
            $table->string('ads-in');
            $table->string('ads-in-in');
            $table->string('liturgies');
            $table->string('created_by');
            $table->string('prayer_cards');
            $table->string('thanks_card');
            $table->string('particularities');

            $table->string('client');
            $table->string('client_address');
            $table->string('client_city');
            $table->string('client_phone');
            $table->string('client_zipcode');
            $table->string('client_relationship_to_deceased');
            $table->string('client_email');
            $table->date('client_sign_date');

            $table->string('additional_wishes_funeral');
            $table->string('additional_wishes_media');

            $table->string('ashes_completion');

            $table->string('signature_client');
            $table->string('signature_contractor');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
