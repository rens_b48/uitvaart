@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-md-12">

    <h2>Volledig overzicht</h2>

    <div class="center_panel overview">
      <div class="panel_links">
         <ul>
            <li>
                <a href="{{ url('/invoer') }}">Aangifte</a>
            </li>
            <li>
                <a href="{{ url('/aanvraagsneek') }}">Aanvraag Uitvaartcentrum &amp; Crematorium Sneek</a>
            </li>
            <li>
                <a href="{{ url('/aanvraagcrematie') }}">Aanvraag voor crematie</a>
            </li>
            <li>
                <a href="{{ url('/aanname_form_begroting') }}">Aanname formulier en begroting</a>
            </li>
        </ul>
     </div>
    </div>

    <div class="search_form">
      {!! Form::open(array('url' => 'overzicht/search', 'method' => 'get')) !!}
        {!! Form::text('naam', null, array('placeholder' => 'Zoek op achternaam')) !!}
        {!! Form::submit('Zoeken') !!}
      {!! Form::close() !!}
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script type="text/javascript">
      $('.search_form').submit(function(){
        var val = $(this).find('input:first').val();
        window.location.href = '//' + window.location.hostname + '/overzicht/search/' + val;
        return false;
      });
    </script>

    <hr>

    <div class="overview">
      <div class="overview_head">
        <span class="id">Id</span>
        <span class="first_name">Voornaam</span>
        <span class="last_name">Achternaam</span>
        <span class="death_date">Overleden op</span>
      </div>

      <hr>

      @foreach ($output as $row)
        <div class="span_table">
            <a href="{{ URL::to('/overzicht/' . $row->deceased_id) }}">
                <span class="id">{{ $row->deceased_id }}</span>
                <span class="first_name">{{ $row->firstname_declarant }}</span>
                <span class="last_name">{{ $row->name_declarant }}</span>
                <span class="death_date">{{ date('d-m-Y', strtotime($row->date_of_death)) }}</span>
            </a>
          </span>
        </div>
      @endforeach

      <?php echo $output->render(); ?>

    </div>
  </div>
</div>


@endsection
