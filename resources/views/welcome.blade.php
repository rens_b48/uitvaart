@extends('layouts.app')

@section('content')
<div class="start-page">
    <div class="container">
        <div class="row">
             <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    
                    @if (Auth::guest())
                        <div class="panel-heading">Welkom </div>
                            <div class="panel-body">
                                U moet inloggen om de formulieren te bekijken.
                            <a href="{{ url('/login') }}">Login</a>
                    </div>
                    @else
                        <div class="center_panel">
                          <div class="panel_links">
                             <ul>
                                <li>
                                    <a href="{{ url('/overzicht') }}">Overzicht bekijken</a>  
                                </li>
                                <li>
                                    <a href="{{ url('/invoer') }}">Aangifte van overlijden</a> 
                                </li>
                                <li>
                                    <a href="{{ url('/aanvraagsneek') }}">Aanvraag voor uitvaart bij Uitvaartcentrum &amp; Crematorium Sneek</a>
                                </li>
                                <li>
                                    <a href="{{ url('/aanname_form_begroting') }}">Aanname formulier en begroting</a>
                                </li>
                            </ul>
                         </div>       
                        </div>
                    @endif
                  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
