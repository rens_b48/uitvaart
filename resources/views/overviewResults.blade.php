@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-md-8 col-md-offset-2">

    <h2>Zoek resultaten</h2>  

	 <div class="search_form">
      {!! Form::open(array('url' => 'overzicht/')) !!}
        {!! Form::text('naam', null, array('placeholder' => 'Zoek op achternaam')) !!}
        {!! Form::submit('Zoeken') !!}
      {!! Form::close() !!}
    </div>

    <div class="page_link">
    	<a href="{{ URL::to('/overzicht') }}">Terug naar volledig overzicht</a>
    </div>

    <hr>    

    <div class="overview">
      <div class="overview_head">
        <span class="id">Id</span>
        <span class="first_name">Voornaam</span>
        <span class="last_name">Achternaam</span>
        <span class="death_date">Overleden op</span>
      </div>

      <hr>

	 	@foreach ($search_results as $result)
	    	<div class="span_table">
	            <a href="{{ URL::to('/overzicht/' . $result['deceased_id']) }}">
	                <span class="id">{{ $result ['deceased_id'] }}</span>
	                <span class="first_name">{{ $result ['firstname_declarant'] }}</span>
	                <span class="last_name">{{ $result ['name_declarant'] }}</span>
	                <span class="death_date">{{ date('d-m-Y', strtotime($result ['date_of_death'])) }} </span>
                  
	            </a>
	   	 	</div>
	 	@endforeach

	</div>
  </div>
</div>
@endsection