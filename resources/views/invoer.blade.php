@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-md-8 col-md-offset-2">
    <h2>Aangifte van overlijden</h2>
    <div class="main_form">
        <form method="POST">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <?php var_dump($form) ?>
        @foreach($form as $field)
          @if(!empty($field['label']))
            <b>{{ $field['label'] }}</b>
          @endif
          <table>

            @foreach($field['fields'] as $field)
              @if($field[2] == 'radio')
              <tr>
                <td>{{ $field[0] }}</td>
                <td>
                @foreach($field[3] as $key => $val)
                  <input type="{{ $field[2] }}" name="{{ $field[1] }}" value="{{ $key }}" />{{ $val }}
                @endforeach
                </td>
              </tr>
              @else
                <tr><td>{!! $field[0] !!}</td><td><input type="{{ $field[2] }}" name="{{ $field[1] }}"  /></td></tr>
              @endif
            @endforeach

          </table>
          <hr>

        @endforeach



        <!-- <b>Overledene</b>
            <table>
              <tr>
                <td>Akte nr.</td><td><input type="text" name="certificate_number" /></td>
              </tr>
              <tr>
                <td>Gemeente</td>
                <td><input type="text" name="town" /></td>
              </tr>
              <tr>
                <td>Datum en uur van overlijden <br><i>24 uur-tijdaanduiding gebruiken</i></td>
                <td><input type="date" name="date" /></td>
                <td><input type="text" name="date_hour" /></td>
                <td>uur</td>
              </tr>
                <td>Naam en voornaam</td>
                <td><input type="text" name="firstname"></td>
              </tr>
              <tr>
                <td>Geboortegemeente en datum</td>
                <td><input type="text" name="birthcity_and_date" /></td>
              </tr>
              <tr>
                <td>Woongemeente</td>
                <td><input type="text" name="living_commune" /></td>
              </tr>
              <tr>
                <td>Adres aldaar</td>
                <td><input type="text" name="address" /></td>
              </tr>
              <tr>
                <td>Beroep</td>
                <td><input type="text" name="profession" /></td>
              </tr>
              <tr>
                <td>Ongehuwd / gehuwd met</td>
                <td><input type="text" name="unmarried_married_to" /></td>
              </tr>
              <tr>
                <td>Weduwnaar van/ weduwe van/ gescheiden van<br><i>(naam en voornaam)</i></td>
                <td><input type="text" name="widow_of" /></td>
              </tr>
              <tr>
                <td>Naam en voornaam vader</td>
                <td><input type="text" name="name_father" /></td>
              </tr>

              <tr>
                <td>overleden</td>
                <td>
                <input type="radio" name="father_dead" value="ja" />Ja <input type="radio" name="father_dead" value="nee" />Nee
                </td>
              </tr>
              <tr>
                <td>Naam en voornaam moeder</td>
                <td><input type="text" name="name_mother" /></td>
              </tr>

              <tr>
                <td>overleden</td><td>
               <span> <input type="radio" name="mother_dead" value="ja" />Ja </span><input type="radio" name="mother_dead" value="nee" />Nee</td>
              </tr>
            </table>

             <hr>

            <b>Aangever</b>
            <table>
              <tr>
                <td>Naam</td>
                <td><input type="text" name="name_declarant" /></td>
              </tr>
              <tr>
                <td>Voornamen</td>
                <td><input type="text" name="firstnames_declarant" /></td>
              </tr>
              <tr>
                <td>Geboortedatum en geboortegemeente</td>
                <td><input type="birthdate_birthcommune" /></td>
              </tr>
              <tr>
                <td>Woonplaats</td>
                <td><input type="text" name="city" /></td>
              </tr>
            </table>

            <hr>

            <p>De begrafenis / crematie vindt plaats op <input type="text" name="funeral_day" />dag
            <input type="text" name="funeral_date" /> 20<input type="text" name="funeral_year" /> om
            <input type="text" name="funeral_hour" />uur op de
            <input type="text" name="cemetary_location" />begraafplaats / in het crematorium te
            <input type="text" name="crematorium_location" /></p>

            <hr>

            <table>
              <tr>
                <td>Naam van meerderjarige erfgenaam</td>
                <td><input type="text" name="adult_heir_name" /></td>
              </tr>
              <tr>
                <td>Adres</td>
                <td><input type="text" name="heir_address" /></td>
              </tr>
              <tr>
                <td>Woonplaats</td>
                <td><input type="text" name="heir_city" /></td>
              </tr>
              <tr>
                <td>Laat de overledene kinderen na</td>
                <td class="fullwidth"><input type="text" name="adult_heir_children" />Meerderjarige
                <input type="text" name="minor_heir_children" />Minderjarige kinderen</td>
              </tr>
            </table>
   -->
        <input type="submit" value="Volgende">

        </form>
        <p>Uittreksel van akte van overlijden: <b>ja 1 x</b></p>
    </div>
  </div>
  </div>
@endsection
