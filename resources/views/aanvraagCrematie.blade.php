@extends('layouts.app')

@section('content')
<div class="container">
          <h2>Aanvraag voor crematie</h2>
          <h2>Crematorium Goutum</h2>
          <div class="main_form">
              <form method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

        @foreach($form as $field)
          @if(!empty($field['label']))
            <b>{!! $field['label'] !!}</b>
          @endif
          <table>

          @foreach($field['fields'] as $field)
            @if($field[2] == 'radio')
            <tr>
              <td>{{ $field[0] }}</td>
              <td>
              @foreach($field[3] as $key => $val)
                <input type="{{ $field[2] }}" name="{{ $field[1] }}" value="{{ $key }}" />{!! $val !!}
              @endforeach
              </td>
            </tr>
            @elseif($field[2] == 'checkbox')
              <tr>
                <td>{{ $field[0] }}</td>
                <td>
                @foreach($field[3] as $key => $val)
                  <input type="{{ $field[2] }}" name="{{ $field[1] }}" value="{{ $key }}" />{!! $val !!}
                @endforeach
                </td>
              </tr>
            @elseif($field[2] == 'p')
              <tr><td>{!! $field[0] !!}</td></tr>
            @elseif($field[2] == 'textarea')
              <tr><td>{!! $field[0] !!}</td><td><textarea name="{{ $field[1] }}" data-validate="{{ ( !empty($field[3]) && !is_array($field[3]) ? $field[3] : '') }}" rows="2"></textarea></tr>
            @else
              <tr><td>{!! $field[0] !!}</td><td><input type="{{ $field[2] }}" name="{{ $field[1] }}" data-validate="{{ ( !empty($field[3]) && !is_array($field[3]) ? $field[3] : '') }}" /></td></tr>
            @endif
          @endforeach

            </table>
            <hr>

        @endforeach

              <input type="submit" value="Volgende">

              </form>
          </div>
        </div>
  </div>
@endsection
