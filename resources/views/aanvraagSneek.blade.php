@extends('layouts.app')

@section('content')
<div class="container">
<?php 

$values = [];
$placeholder = "";
foreach ($DBDataGeneral[0] as $key => $value) {
  $values[$key] = $value;
}

?>
          <h2>Aanvraag voor uitvaart bij Uitvaartcentrum &amp; Crematorium Sneek</h2>
          <div class="main_form">
              <form method="POST">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

        @foreach($form as $field)
          @if(!empty($field['label']))
            <b>{!! $field['label'] !!}</b>
          @endif
          <table>

          @foreach($field['fields'] as $field)
          <?php
            if (!isset($values[$field[1]])) {
              $value = "";
            } else {
              $value = $values[$field[1]];
            }
          ?>
            @if($field[2] == 'radio')
            <tr>
              <td>{{ $field[0] }}</td>
              <td>
              @foreach($field[3] as $key => $val)
                <input type="{{ $field[2] }}" name="{{ $field[1] }}" value="{{ $key }}" />{!! $val !!}
              @endforeach
              </td>
            </tr>
            @elseif($field[2] == 'checkbox')
              <tr>
                <td>{{ $field[0] }}</td>
                <td>
                @foreach($field[3] as $key => $val)
                  <input type="{{ $field[2] }}" name="{{ $field[1] }}" value="{{ $key }}" />{{ $val }}
                @endforeach
                </td>
              </tr>
            @elseif($field[2] == 'p')
              <tr><td>{!! $field[0] !!}</td></tr>
            @elseif($field[2] == 'textarea')
              <tr><td>{!! $field[0] !!}</td><td><textarea name="{{ $field[1] }}" data-validate="{{ ( !empty($field[3]) && !is_array($field[3]) ? $field[3] : '') }}" rows="2">{{ $value }}</textarea></tr>
            @else
              <tr><td>{!! $field[0] !!}</td><td><input type="{{ $field[2] }}" value="{{ $value }}" name="{{ $field[1] }}" data-validate="{{ ( !empty($field[3]) && !is_array($field[3]) ? $field[3] : '') }}" /></td></tr>
            @endif
          @endforeach

            </table>
            <hr>

        @endforeach

              <input type="submit" value="Opslaan">

              </form>
          </div>

          <h1>Belangrijke informatie voor de opdrachtgever</h1>

          <h2>Algemeen</h2>

          <p>De opdrachtgever is de contactpersoon die de coördinatie van de uitvaart op zich neemt. Deze persoon is ook degene die de
            keuze van asbestemming bepaalt.
            Een aanvraag voor een crematie wordt uitsluitend geaccepteerd indien het formulier volledig is ingevuld en is ondertekend
            door zowel de opdrachtgever van de crematie als de uitvaartverzorger.
            Veranderingen in de wensen die via dit formulier zijn kenbaar gemaakt, kunnen tijdig schriftelijk met handtekening van de
            opdrachtgever bij het crematorium worden doorgegeven. Uitvaartcentrum &amp; Crematorium Sneek stelt zich niet aansprakelijk
            voor de gevolgen die uit een onjuiste beantwoording of incomplete invulling van de aan de voorzijde/voorgaande pagina
            gestelde vragen voortvloeien.
          </p>

          <h2>De plechtigheid</h2>

          <p>
            Voor de totale plechtigheid kunt u standaard 2,5 uur gebruik maken van de faciliteiten van Uitvaartcentrum &amp; Crematorium
            Sneek. Als u veel mensen verwacht, uitgebreide muziekwensen en/of veel sprekers heeft of om een andere reden meer tijd
            wilt hebben voor het afscheid, dan kunt u extra tijd (per half uur) reserveren tegen meerkosten.
            U kunt de plechtigheid inrichten naar uw eigen ideeën. Uitvaartcentrum &amp; Crematorium Sneek zal in de meeste gevallen aan
            uw wensen kunnen voldoen. Bijzonderheden kunt u overleggen met uw uitvaartverzorg(st)er. Indien u dit wenst, is het mogelijk
            om aan het einde van de plechtigheid in het bijzijn van u en uw gasten de kist aan het oog te laten onttrekken. Dit gebeurt bij
            ons door middel van het dalen van de kist.
          </p>

          <h2>De muziek</h2>

          <p>
            Muziek geeft de plechtigheid een persoonlijke uitstraling. De uitvaartverzorg(st)er weet van de meeste muziek of deze
            aanwezig is in Uitvaartcentrum &amp; Crematorium Sneek.
            Uiteraard kunt u ook eigen cd’s of dvd’s meenemen of live muziek laten verzorgen. Uw eigen muziek dient uiterlijk 24 uur vóór
            aanvang van de plechtigheid aanwezig te zijn bij Uitvaartcentrum &amp; Crematorium Sneek zodat we deze kunnen testen.
            Uitvaartcentrum &amp; Crematorium Sneek kan niet aansprakelijk worden gesteld voor de uitvoering van de muziek wanneer deze
            te laat (minder dan 24 uur voor de plechtigheid) is aangeleverd. Speciale wensen voor de live muziek kunt u bespreken met
            uw uitvaartverzorg(st)er.
          </p>

          <h2>De lounge</h2>

          <p>
            Na de plechtigheid in de aula heeft u de beschikking over onze lounge. Hier hebben uw gasten de gelegenheid tot condoleren
          en kunt u hen een consumptie aanbieden (koffie, thee, frisdranken, cake, koek, broodjes, sandwiches, soep, etc.). Ook aan
          speciale wensen met betrekking tot catering kunnen we veelal voldoen. Omdat wij met dagverse producten werken is het van
          belang dat uiterlijk 48 uur vóór aanvang van de plechtigheid de gewenste consumpties aan ons worden doorgegeven.
          De consumpties worden op de volgende wijze in rekening gebracht:<br>
          - Dranken: werkelijk gebruik.<br>
          - Etenswaren en speciale wensen: bestelde hoeveelheid c.q. het werkelijk hoger gebruik. Dit in verband met de bederfelijkheid
          van consumpties.<br>
          - De gebruikte consumpties in de familiekamer en de ontvangstruimte worden ook in rekening gebracht.<br>
          - Overgebleven uitgeserveerde drank- en etenswaren kunnen niet mee naar huis genomen worden.<br>
          - Zelf meegebrachte consumpties worden niet geaccepteerd.
          </p>

          <h2>Opname plechtigheid</h2>

          <p>
            We maken altijd een DVD-opname van de plechtigheid. Deze opname krijgen de nabestaanden mee bij het verlaten van het
            crematorium tenzij vooraf expliciet aangegeven wordt dat deze opname niet gewenst is. Mocht de opname nog niet klaar zijn
            dan zal deze worden opgestuurd.
          </p>

          <h2>Bloemen, linten en kaarten</h2>

          <p>
            U kunt aangeven wat u wilt doen met de bloemen. Indien u dit niet doet dan plaatsen wij de bloemen van de plechtigheid op
            het bloemenveld nabij het parkeerterrein van Uitvaartcentrum &amp; Crematorium Sneek. De kaarten en linten halen wij er af en
            deze ontvangt u van de uitvaartverzorg(st)er.
          </p>

          <h2>Bijzondere voorwerpen in de kist</h2>

          <p>
            Soms willen mensen iets meegeven in de kist van de overledene. Wij verzoeken u met de uitvaartverzorg(st)er te overleggen
            wat er wel en niet meegegeven kan worden. De opbrengst van eventueel mee gegeven (edel)metalen schenken wij via het
            J.C. Vaillantfonds aan diverse goede doelen.
          </p>

          <h2>Bestemming van de as</h2>

          <p>
            Wij bewaren de as tot een maand na de crematiedatum in Uitvaartcentrum &amp; Crematorium Sneek. Dit is een wettelijke
            regeling. Tijdens deze periode ontvangt de opdrachtgever of uitvaartverzorger informatie over de mogelijkheden van de
            bestemming van de as. De opdrachtgever of uitvaartverzorger laat Uitvaartcentrum &amp; Crematorium Sneek weten wat de
            bestemming van de as gaat worden. Als deze wens na 10 jaar niet is gecommuniceerd aan Uitvaartcentrum &amp; Crematorium
            Sneek, vervalt het recht op de as. Dan verstrooien de medewerkers van Uitvaartcentrum &amp; Crematorium Sneek de as zonder
            de familie hiervan in kennis te stellen. Nog niet verrekende kosten voor het bewaren van de as zullen worden berekend aan de
            opdrachtgever. Geef bij verhuizing uw nieuwe adres door.
          </p>

        </div>
  </div>
@endsection
