<html>
<head>
	<title>PDF uitdraai</title>
	<link type="text/css" href="css/stylePDF.css" rel="stylesheet" />
</head>
<body>
	<div class="pdf_page">
    <div class="heslinga_adres">
      Heslinga Uitvaartzorg<br>
      It Fabryk 12<br>
      8621 JD Heeg<br><br>
      tel. 0515-431875<br>
      tel. 06-53777753<br>
      e. info@heslinga-uitvaartverzorging.nl
    <div>
	 
    <div class="table_id">
      <h2>Aangifte uitvaart bij Uitvaartzorg Heslinga</h2>
        <table>
          <tr>
            <th>Overledene</th>
            <th></th>
          </tr>
          <tr>
            <td>Database id.</td>
            <td>{{ $row->deceased_id }}</td> 
          </tr>
          <tr>
            <td>Akte nr.</td>
            <td>{{ $row->serial_number }}</td> 
          </tr>
          <tr>
            <td>Plaats van overlijden</td>
            <td>{{ $row->city_of_death }}</td> 
          </tr>
          <tr>
            <td>Datum van overlijden</td>
            <td>{{ $row->date_of_death }}</td> 
          </tr>
          <tr>
            <td>Naam</td>
            <td>{{ $row->name_declarant }}</td> 
          </tr>
          <tr>
            <td>Voornaam</td>
            <td>{{ $row->firstname_declarant }}</td> 
          </tr>
          <tr>
            <td>Geboorteplaats</td>
            <td>{{ $row->city_of_birth }}</td> 
          </tr>
          <tr>
            <td>Geboorte datum</td>
            <td>{{ $row->date_of_birth }}</td> 
          </tr>
          <tr>
            <td>Huidige woonplaats</td>
            <td>{{ $row->city_declarant }}</td> 
          </tr>
          <tr>
            <td>Burgelijke staat</td>
            <td>{{ $row->maritual_status }}</td> 
          </tr>
          <tr>
            <td>Naam vader</td>
            <td>{{ $row->father }}</td>
          </tr>
          <tr> 
            <td>Vader Overleden</td>
            <td>{{ $row->father_dead }}</td> 
          </tr>
          <tr>
            <td>Naam moeder</td>
            <td>{{ $row->mother }}</td> 
          </tr>
          <tr>
            <td>Moeder Overleden</td>
            <td>{{ $row->mother_dead }}</td> 
          </tr>
             <tr>
            <td>Meerderjarig overledene kinderen</td>
            <td>{{ $row->adult_heir_children }}</td> 
          </tr>
          <tr>
            <td>Minderjarige overleden kinden</td>
            <td>{{ $row->minor_heir_children }}</td> 
          </tr>
        </table>
   
        <table>
          <tr>
            <th>Aangever</th>
            <th></th>
          </tr>
          <tr>
            <td>Aangever naam</td>
            <td>{{ $row->client }}</td> 
          </tr>
          <tr>
            <td>Woonplaats</td>
            <td>{{ $row->client_city }}</td> 
          </tr>
          <tr>
            <td>Adres</td>
            <td>{{ $row->client_address }}</td> 
          </tr>
          <tr>
            <td>Telefoon nr.</td>
            <td>{{ $row->client_phone }}</td> 
          </tr>
        </table>
        
        <table>
          <tr>
            <th>Uitvaart datum</th>
            <th></th>
          </tr>
          <tr>
            <td>Begravenis vindt plaats op</td>
            <td>{{ $row->date_funeral }}</td> 
          </tr>
          <tr>
            <td>Begint om</td>
            <td>{{ $row->memorial_service_time }}</td> 
          </tr>
          <tr>
            <td>Locatie</td>
            <td>{{ $row->memorial_service_location}}</td> 
          </tr>
        </table>
      </div>
    </div>
</body>
</html>