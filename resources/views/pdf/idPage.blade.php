@extends('layouts.app')

@section('content')
<div class="container">
  <div class="col-md-8 col-md-offset-2">

    <div class="id_page">

      <h3>{{ $row->firstname_declarant }} {{ $row->name_declarant }} - {{ date('d-m-Y', strtotime($row->date_of_death)) }}</h3>

      <hr>

      <div class="table_id">
        <table>
          <tr>
            <td>Id</td>
            <td>{{ $row->deceased_id }}</td> 
          </tr>
           <tr>
            <td>Achternaam</td>
            <td>{{ $row->name_declarant }}</td> 
          </tr>
           <tr>
            <td>Voornaam</td>
            <td>{{ $row->firstname_declarant }}</td> 
          </tr>
           <tr>
            <td>Overlijdensdatum</td>
            <td>{{ date('d-m-Y', strtotime($row->date_of_death)) }}</td> 
          </tr>
        </table>
      </div>

      <hr>

      <div class="edit">
        <p>Aangifte Heslinga</p>
          <div clas="edit_button"> 
            <a href=""><i class="material-icons">build</i></a>
            <a href="{{ URL::to('/pdfHeslinga/' . $row->deceased_id ) }}"><i class="material-icons">print</i></a>
          </div>
      </div>

      <div class="edit">
         <p>Aanvraag Sneek</p>
          <div clas="edit_button">
          <a href="{{ URL::to('/aanvraagsneek/' . $row->deceased_id ) }}"><i class="material-icons">build</i></a> 
            <a href="{{ URL::to('/pdfSneek/' . $row->deceased_id ) }}"><i class="material-icons">print</i></a>
          </div>
      </div>

      <div class="edit">
         <p>Aanvraag Goutum</p>
          <div clas="edit_button"> 
            <a href=""><i class="material-icons">build</i></a>
            <a href="{{ URL::to('/pdfGoutum/' . $row->deceased_id ) }}"><i class="material-icons">print</i></a>
          </div>
      </div>

      <div class="edit">
         <p>Overledene verwijderen</p>
          <div clas="edit_button"> 
            <a href=""><i class="material-icons">delete</i></a>
          </div>
      </div>

    </div>
      
  </div>
</div>
@endsection