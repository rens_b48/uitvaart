<html>
<head>
	<title>PDF uitdraai</title>
	<link type="text/css" href="css/stylePDF.css" rel="stylesheet" />
</head>
<body>
	<div class="pdf_page">
    <div class="heslinga_adres">
      Heslinga Uitvaartzorg<br>
      It Fabryk 12<br>
      8621 JD Heeg<br><br>
      tel. 0515-431875<br>
      tel. 06-53777753<br>
      e. info@heslinga-uitvaartverzorging.nl
    <div>

    <div class="table_id">
      <h2>Aanvraag voor uitvaart<br> Uitvaart & Crematorium Sneek</h2>
        <table>
          <tr>
            <th>Aanvraag voor Rouwdienst Crematieplechtigheids</th>
            <th></th>
          </tr>
          <tr>
            <td>Rouwdienst</td>
            <td>{{ $row->funeral_time }}</td> 
          </tr>
          <tr>
            <td>Crematieplechtigheid</td>
            <td>{{ $row->cremation_time }}</td> 
          </tr>
          <tr>
            <td>Datum</td>
            <td>{{ $row->date_funeral }}</td> 
          </tr>
          <tr>
            <td>Aanvang</td>
            <td>{{ $row->memorial_service_time }}</td> 
          </tr>
          <tr>
            <td>Aantal te verwachten personen</td>
            <td>{{ $row->visitors_count }}</td> 
          </tr>
          <tr>
            <td>Standaard tijd 2:30 uur</td>
            <td></td> 
          </tr>
          <tr>
            <td>Extra tijd (na overleg)</td>
            <td>min.</td> 
          </tr>
        </table>
        <table>
          <tr>
            <th>Persoonsgegevens overledene</th>
            <th></th> 
          </tr>
          <tr>
            <td>Naam</td>
            <td></td> 
          </tr>
          <tr>
            <td>Voornaam</td>
            <td></td> 
          </tr>
          <tr>
            <td>Adres</td>
            <td></td> 
          </tr>
          <tr>
            <td>Postcode</td>
            <td></td>
          </tr>
          <tr> 
            <td>Plaats</td>
            <td></td> 
          </tr>
          <tr>
            <td>Geboorteplaats</td>
            <td></td> 
          </tr>
          <tr>
            <td>Geboortedatum</td>
            <td></td> 
          </tr>
          <tr>
            <td>Overlijdensplaats</td>
            <td></td> 
          </tr>
          <tr>
            <td>Burgerlijke staat</td>
            <td></td> 
          </tr>
        </table>
        <table>
          <tr>
            <th>Bijzonderheden afscheidsdienst</th>
            <th></th> 
          </tr>
          <tr>
            <td>Sprekers</td>
            <td></td> 
          </tr>
          <tr>
            <td>Wenst u liturgische attributen te gebruiken</td>
            <td></td> 
          </tr>
          <tr>
            <td>Kaarsen </td>
            <td></td> 
          </tr>
          <tr>
            <td>Kruis</td>
            <td></td>
          </tr>
          <tr> 
            <td>Anders</td>
            <td></td> 
          </tr>
          <tr>
            <td>Wenst u de baar te laten dalen</td>
            <td></td> 
          </tr>
          <tr>
            <td>Wenst de familie als laatste de aula te verlaten</td>
            <td></td> 
          </tr>
          <tr>
            <td>Wensen t.a.v. de bloemen</td>
            <td></td> 
          </tr>
        </table>
        <table>
          <tr>
            <th>Wensen condoleanceruimte</th>
            <th></th> 
          </tr>
          <tr>
            <td>Wenst u gebruik te maken van de Lounge</td>
            <td></td> 
          </tr>
          <tr>
            <td>Aanvullende wensen/informatie afscheidsdienst</td>
            <td></td> 
          </tr>
        </table>
        <table>
          <tr>
            <th>Wensen beeld en geluid</th>
            <th></th> 
          </tr>
          <tr>
            <td>Muziek</td>
            <td></td> 
          </tr>
          <tr>
            <td>1.</td>
            <td>2.</td> 
          </tr>
          <tr>
            <td>3.</td>
            <td>4.</td> 
          </tr>
          <tr>
            <td>5.</td>
            <td>6.</td> 
          </tr>
          <tr>
            <td>Wenst u beeldmateriaal te presenteren</td>
            <td></td> 
          </tr>
          <tr>
            <td>Wenst u een DVD-opname</td>
            <td></td> 
          </tr>
          <tr>
            <td>Aanvullende wensen/informatie beeld en geluid</td>
            <td></td> 
          </tr>
        </table>
        <table>
          <tr>
            <th>Asbestemming</th>
            <th></th> 
          </tr>
           <tr>
            <td>Asafhandeling rechtstreeks door crematorium met de opdrachtgever</td>
            <td></td> 
          </tr>
          <tr>
            <td>Asafhandeling via de uitvaartverzorger</td>
            <td></td> 
          </tr>
        </table>
        <table>
          <tr>
            <th>Ter attentie van de opdrachtgever</th>
            <th></th> 
          </tr>
           <tr>
            <td>Akkoord met genoemde voorwaarden</td>
            <td></td> 
          </tr>
        </table>
        <table>
          <tr>
            <th>Gegevens opdrachtgever</th>
            <th></th> 
          </tr>
           <tr>
            <td>Achternaam</td>
            <td></td> 
          </tr>
          <tr>
            <td>Voornaam</td>
            <td></td> 
          </tr>
          <tr>
            <td>Postcode</td>
            <td></td> 
          </tr>
          <tr>
            <td>Plaats</td>
            <td></td> 
          </tr>
          <tr>
            <td>Relatie tot de overleden</td>
            <td></td> 
          </tr>
           <tr>
            <td>Datum</td>
            <td></td> 
          </tr>
        </table>
        <table>
          <tr>
            <th>Gegevens uitvaartonderneming/-vereniging (factuuradres)</th>
            <th></th> 
          </tr>
           <tr>
            <td>Naam</td>
            <td></td> 
          </tr>
          <tr>
            <td>Adres</td>
            <td></td> 
          </tr>
          <tr>
            <td>Postcode</td>
            <td></td> 
          </tr>
          <tr>
            <td>Plaats</td>
            <td></td> 
          </tr>
          <tr>
            <td>Uitvaartleider</td>
            <td></td> 
          </tr>
           <tr>
            <td>Telefoonnummer</td>
            <td></td> 
          </tr>
           <tr>
            <td>E-mailadres</td>
            <td></td> 
          </tr>
          <tr>
            <td>Datum</td>
            <td></td> 
          </tr>
        </table>
      </div>
    </div>
</body>
</html>