<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login', function () {
    return view('login');
});

Route::get('overzicht/search/{q}', 'overzichtController@search_results');

Route::get('overzicht', 'overzichtController@index');
Route::get('overzicht/{id}', 'overzichtIdController@index');

Route::get('invoer', 'FormController@index');
Route::post('invoer', 'FormController@getPost');

Route::get('aanvraagsneek', 'FormController@getRequestSneek');
Route::get('aanvraagsneek/{id}', 'FormController@getRequestSneekId');
Route::post('aanvraagsneek', 'FormController@getRequestSneekPost');
Route::post('aanvraagsneek/{id}', 'FormController@postRequestSneekId');

Route::get('aanvraagcrematie', 'FormController@getRequestCremation');
Route::post('aanvraagcrematie', 'FormController@postRequestCremation');

Route::get('aanname_form_begroting', 'FormController@getAannameFormBegroting');
Route::post('aanname_form_begroting', 'FormController@postAannameFormBegroting');

//PDF Route
Route::get('pdfHeslinga/{id}', 'PDFController@getPDF');
Route::get('pdfSneek/{id}', 'PDFController@getPDF2');
Route::get('pdfGoutum/{id}', 'PDFController@getPDF3');

// Authentication routes

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');



Route::auth();

Route::get('/formulieren', 'HomeController@index');
