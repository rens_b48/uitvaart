<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use DB;

class FormController extends Controller
{
    //MAIN FORM ARRAYS
    public static $mainForm = [
        [
            'label' => '',
            'fields' => [
                'uitvaartvereniging' => ['Uitvaartvereniging:', 'funeral_association', 'text', 'varchar:255'],
                'totaalnummer' => ['Totaalnummer:', 'total_number', 'text', 'varchar:255'],
                'volgnummer' => ['Volg nummer:', 'serial_number', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => 'Overledene',
            'fields' => [
                'naam' => ['Naam:', 'name_declarant', 'text', '*:varchar:255'],
                'voornaam' => ['Voornaam:', 'firstname_declarant', 'text', '*:varchar:255'],
                'adres' => ['Adres:', 'address_declarant', 'text', 'varchar:255'],
                'woonplaats' => ['Woonplaats:', 'city_declarant', 'text', 'varchar:255'],
                'geboortedatum' => ['Geboortedatum:', 'date_of_birth', 'date', '*'],
                'leeftijd' => ['Leeftijd:', 'age_declarant', 'text', 'varchar:255'],
                'geboorteplaats' => ['Geboorteplaats:', 'city_of_birth', 'text', 'varchar:255'],
                'burgerlijke_staat' => ['Burgerlijke staat:', 'maritual_status', 'text', 'varchar:255'],
                'vader' => ['Vader:', 'father', 'text', 'varchar:255'],
                'vader_overleden' => ['Overleden:', 'father_dead', 'radio', ['j' => 'Ja', 'n' => 'Nee']],
                'moeder' => ['Moeder:', 'mother', 'text', 'varchar:255'],
                'moeder_overleden' => ['Overleden:', 'mother_dead', 'radio', ['j' => 'Ja', 'n' => 'Nee']],
                'meerderjarige overledene kinderen' => ['Meerderjarige kinderen overledene:', 'adult_heir_children', 'text', 'varchar:255'],
                'minderjarige overledene kinderen' => ['Minderjarige kinderen overledene:', 'minor_heir_children', 'text', 'varchar:255'],
                'datum van overlijden' => ['Datum van overlijden:', 'date_of_death', 'date', ''],
                'uur overlijden' => ['Uur van overlijden.<br><i>24 uur-tijdaanduiding gebruiken</i>:', 'hour_of_death', 'time', 'varchar:255'],
                'overlijdensplaats' => ['Plaats van overlijden:', 'city_of_death', 'text', 'varchar:255'],
                'huisarts' => ['Huisarts:', 'doctor', 'text', 'varchar:255'],
                'telefoon' => ['Telefoon:', 'telephone', 'text', 'varchar:255'],
            ]
        ],
        [
            'label' => 'Opbaring',
            'fields' => [
                'opbaring_aula' => ['Opbaring aula:', 'lying_in_state_auditorium', 'text', 'varchar:255'],
                'opbaring_van' => ['Opbaring van:', 'lying_in_state_start', 'date', ''],
                'opbaring_tot' => ['Opbaring tot:', 'lying_in_state_end', 'date', ''],
                'laatste_zorg' => ['Laatste zorg:', 'final_care', 'text', 'varchar:255'],
            ]
        ],
        [
            'label' => 'Kist',
            'fields' => [
                'kist_nummer' => ['Kist nr:', 'coffin_number', 'text', 'varchar:255'],
                'kist_lengte' => ['Lengte:', 'coffin_length', 'text', 'varchar:255'],
                'draagbaar' => ['Draagbaar:', 'coffin_carriable', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'hoes' => ['Hoes:', 'coffin_slipcover', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'kruis' => ['Kruis:', 'cross', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'koeldeksel' => ['Koeldeksel:', 'coffin_cool_lid', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'koelplaat' => ['Koelplaat:', 'coffin_heatsink', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'airco' => ['Airco:', 'coffin_airco', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
            ]
        ],
        [
            'label' => 'Drukwerk',
            'fields' => [
                'drukwerk' => ['Drukwerk:', 'press-work', 'text', 'varchar:255'],
                'aantal' => ['Aantal:', 'presswork-amount', 'text', 'varchar:255'],
                'port' => ['Porto:', 'postage-amount', 'text', 'varchar:255'],
                'eu' => ['Eu:', 'eu', 'text', 'varchar:255'],
                'w' => ['W:', 'w', 'text', 'varchar:255'],
                'drukker' => ['Drukker:', 'printer', 'text', 'varchar:255'],
                'advertenties_in' => ['Advertenties in:', 'ads-in', 'text', 'varchar:255'],
                'advertenties_in_in' => ['In:', 'ads-in-in', 'text', 'varchar:255'],
                'liturgiën' => ['Liturgiën:', 'liturgies', 'text', 'varchar:255'],
                'gemaakt_door' => ['Gemaakt door:', 'created_by', 'text', 'varchar:255'],
                'bidprentjes' => ['Bidprentjes:', 'prayer_cards', 'text', 'varchar:255'],
                'bedankkaarten' => ['Bedankkaarten:', 'thanks_card', 'text', 'varchar:255'],
                'bijzonderheden' => ['Bijzonderheden:', 'particularities', 'textarea', 'varchar:255']
            ]
        ],
        [
            'label' => 'Opdrachtgever',
            'fields' => [
                'opdrachtgever' => ['Opdrachtgever:', 'client', 'text', 'varchar:255'],
                'opdrachtgever_adres' => ['Adres:', 'client_address', 'text', 'varchar:255'],
                'woonplaats' => ['Woonplaats:', 'client_city', 'text', 'varchar:255'],
                'telefoon' => ['Telefoon:', 'client_phone', 'text', 'varchar:255'],
                'tekst' => ['Ondergetekende stelt zich bij deze  aansprakelijk voor de kosten welke uit deze opdracht zullen voortvloeien. De genoemde bedragen zijn een raming " en kunnen derhalve geen rechten aan worden ontleend.', '', 'p', ''],
            ]
        ],

        [
            'label' => '',
            'fields' => [
                'handtekening_opdrachtgever' => ['Handtekening opdrachtgever:', 'signature_client', 'text', 'varchar:255'],
                'handtekening_opdrachtnemer' => ['Handtekening opdrachtnemer:', 'signature_contractor', 'text', 'varchar:255'],
            ]
        ]
    ];

    public static $costsForm = [
        [
            'label' => 'Kosten',
            'fields' => [
                'opname_en_uitvoer' => ['Opname en uitvoer:', 'recordings_and_output_costs', 'text', 'varchar:255'],
                'laatste_zorg' => ['Laatste verzorging:', 'final_care_costs', 'text', 'varchar:255'],
                'autokosten' => ['Autokosten:', 'car_costs', 'text', 'varchar:255'],
                'telefoonkosten' => ['Telefoonkosten:', 'phone_costs', 'text', 'varchar:255'],
                'dragers' => ['Dragers:', 'carrier_costs', 'text', 'varchar:255'],
                'inhoudingen1' => ['Inhoudingen:', 'deductions1', 'text', 'varchar:255'],
                'onk_dragers' => ['Onk. dragers:', 'onk_carriers', 'text', 'varchar:255'],
                'grafdelven' => ['Grafdelven:', 'grave_digging_costs', 'text', 'varchar:255'],
                'inhoudingen2' => ['Inhoudingen:', 'deductions2', 'text', 'varchar:255'],
                'huur_graven' => ['Huur graven:', 'rent_graves', 'text', 'varchar:255'],
                'huur_baar' => ['Huur baar:', 'rent_bier', 'text', 'varchar:255'],
                'zinktoestel' => ['Zinktoesten:', 'sink_device_costs', 'text', 'varchar:255'],
                'klokluiden' => ['Klokluiden:', 'bell_ringing_costs', 'text', 'varchar:255'],
                'inhoudingen3' => ['Inhoudingen:', 'deductions3', 'text', 'varchar:255'],
                'fooi' => ['Fooi:', 'tip', 'text', 'varchar:255'],
                'kist' => ['Kist:', 'coffin_costs', 'text', 'varchar:255'],
                'rouwwagen' => ['Rouwwagen:', 'mourning_car_costs', 'text', 'varchar:255'],
                'koeling' => ['Koeling:', 'cooling_costs', 'text', 'varchar:255'],
                'huur_rouwgoed' => ['Huur rouwgoed:', 'good_grief_costs', 'text', 'varchar:255'],
                'draagbaar' => ['Draagbaar:', 'carry_bier_costs', 'text', 'varchar:255'],
                'cross' => ['Kruis:', 'cross_costs', 'text', 'varchar:255'],
                'stikker_en_doc' => ['Stikker en doc.:', 'sticker_and_doc_costs', 'text', 'varchar:255'],
                'volgwagens' => ['Volgwagens:', 'following_car_costs', 'text', 'varchar:255'],
                'condoleance_boekje' => ['Condoleance boekje:', 'condolances_book_costs', 'text', 'varchar:255'],
                'akte_van_overlijden' => ['Akte van overlijden:', 'death_certificate_costs', 'text', 'varchar:255'],
                'cd_dvd_opname' => ['cd/dvd-opname:', 'cd_dvd_recording_costs', 'text', 'varchar:255'],
                'opbaring' => ['Opbaring:', 'lying_in_state_costs', 'text', '*:varchar:255'],
                'ziekenhuis' => ['Ziekenhuis:', 'hospital_costs', 'text', 'varchar:255'],
                'drukwerk_brief' => ['Drukwerk brief:', 'printed_letter_costs', 'text', 'varchar:255'],
                'port' => ['Porto:', 'postage_costs', 'text', 'varchar:255'],
                'liturgien' => ['Liturgiën:', 'liturgies_costs', 'text', 'varchar:255'],
                'verzendset' => ['Verzendset:', 'sendset_costs', 'text', 'varchar:255'],
                'advertentie_LC' => ['Advertentie LC:', 'ad_LC_costs', 'text', 'varchar:255'],
                'advertentie_FD' => ['Advertentie FD:', 'ad_FD_costs', 'text', 'varchar:255'],
                'sneeker_niews' => ['Sneeker Nieuws:', 'sneeker_niews_ad_costs', 'text', 'varchar:255'],
                'other_paper' => ['Andere Krant:', 'other_paper_costs', 'text', 'varchar:255'],
                'bedankkaartjes' => ['Bedankkaartjes:', 'thanks_card_costs', 'text', 'varchar:255'],
                'advertenties' => ['Advertenties:', 'ad_costs', 'text', 'varchar:255'],
                'bidprentjes' => ['Bidprentjes:', 'prayer_cards_costs', 'text', 'varchar:255'],
                'faxen' => ['Faxen:', 'faxing_costs', 'text', 'varchar:255'],
                'geluidsinst' => ['Geluidsinst:', 'sound_inst', 'text', 'varchar:255'],
                'huur_rouwg' => ['Huur rouwg:', 'good_grief_rent', 'text', 'varchar:255'],
                'bedkoeling' => ['Bedkoeling:', 'bed_cooler_costs', 'text', 'varchar:255'],
                'airco' => ['Airco:', 'airco_costs', 'text', 'varchar:255'],
                'naamplaatje' => ['Naamplaatje:', 'nameplate', 'text', 'varchar:255'],
                'huur_kerk_zaal' => ['Huur kerk/zaal:', 'rent_church_room', 'text', 'varchar:255'],
                'koster' => ['Koster:', 'sacristan_costs', 'text', 'varchar:255'],
                'onk_pred' => ['Onk pred.:', 'onk_pred_costs', 'text', 'varchar:255'],
                'organisatie' => ['Organisatie:', 'organisation_costs', 'text', 'varchar:255'],
                'spreker' => ['Spreker:', 'speaker_costs', 'text', 'varchar:255'],
                'crematie' => ['Crematie:', 'cremation_costs', 'text', 'varchar:255'],
                'asafhandeling' => ['Asafhandeling:', 'ashes_completion_costs', 'text', 'varchar:255'],
                'navisite' => ['Navisite:', 'after_visitors_costs', 'text', 'varchar:255'],
                'bloemen' => ['Bloemen:', 'flower_costs', 'text', '*:varchar:255'],
                'bijzonderheden' => ['Bijzonderheden:', 'particularities_costs', 'text', 'varchar:255'],
                'onvoorzien' => ['Onvoorzien:', 'overlook_costs', 'text', 'varchar:255'],
                'administratie_kosten' => ['Administratie kosten:', 'administration_costs', 'text', '*:varchar:255']
            ]
        ],
        [
            'label' => 'Verzekerings polissen',
            'fields' => [
                'polis1:' => ['Polis 1', 'insurance_policy_1', 'text', 'varchar:255'],
                'uitk1' => ['Uitk-1:', 'payment_1', 'text', 'varchar:255'],
                'polis2:' => ['Polis 2', 'insurance_policy_2', 'text', 'varchar:255'],
                'uitk2' => ['Uitk-2:', 'payment_2', 'text', 'varchar:255'],
                'polis3:' => ['Polis 3', 'insurance_policy_3', 'text', 'varchar:255'],
                'uitk3' => ['Uitk-3:', 'payment_3', 'text', 'varchar:255'],
                'polis4:' => ['Polis 4', 'insurance_policy_4', 'text', 'varchar:255'],
                'uitk4' => ['Uitk-4:', 'payment_4', 'text', 'varchar:255'],
            ]
        ],
        [
            'label' => '',
            'fields' => [
                'totaal' => ['<b>Totaal:</b>', 'total', 'text', 'varchar:255'],
                'ver_korting' => ['Ver. korting', 'ver_discount', 'text', 'varchar:255'],
                'uitk_korting' => ['Uitk. Verz.', 'payment_discount', 'text', 'varchar:255'],
                'te_betalen' => ['Te betalen', 'to_pay', 'text', 'varchar:255'],
            ]
        ]
    ];

    public static $funeralForm = [
        [
            'label' => 'Uitvaart',
            'fields' => [
                'datum_uitvaart' => ['Datum uitvaart:', 'date_funeral', 'date', '*'],
                'condoleantiemoment1_datum' => ['Datum condoleantie moment 1:', 'condolences_moment_1_date', 'date'],
                'condoleantiemoment1_tijd' => ['Tijd condoleantie moment 1:', 'condolences_moment_1_time', 'time'],
                'condoleantiemoment1_locatie' => ['Locatie condoleantie moment 1:', 'condolences_moment_1_location', 'text', 'varchar:255'],
                'condoleantiemoment2_datum' => ['Datum condoleantie moment 2:', 'condolences_moment_2_date', 'date'],
                'condoleantiemoment2_tijd' => ['Tijd condoleantie moment 2:', 'condolences_moment_2_time', 'time'],
                'condoleantiemoment2_locatie' => ['Locatie condoleantie moment 2:', 'condolences_moment_2_location', 'text', 'varchar:255'],
                'rouwdienst_locatie' => ['Locatie rouwdienst:', 'memorial_service_location', 'text', '*:varchar:255'],
                'rouwdienst_tijd' => ['Tijd rouwdienst:', 'memorial_service_time', 'time', ''],
                'begravenis_locatie' => ['Locatie begravenis:', 'funeral_location', 'text', 'varchar:255'],
                'begravenis_tijd' => ['Tijd begravenis:', 'funeral_time', 'time'],
                'crematie_locatie' => ['Locatie crematie:', 'cremation_location', 'text', 'varchar:255'],
                'crematie_tijd' => ['Tijd crematie:', 'cremation_time', 'time'],
                'as_bestemming' => ['As bestemming:', 'ashes_destination', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => '',
            'fields' => [
                'voorganger' => ['Voorganger:', 'predecessor', 'text', 'varchar:255'],
                'organist' => ['Organist:', 'organist', 'text', 'varchar:255'],
            ]
        ],
        [
            'label' => '',
            'fields' => [
                'rouwauto_van' => ['Rouwauto van:', 'mourning_car', 'text', 'varchar:255'],
                'volgauto_van' => ['Volgauto van:', 'support_car', 'text', 'varchar:255'],
                'volgauto_aantal' => ["Aantal volgauto's:", 'support_car_count', 'text', 'varchar:255'],
                'familie_afgehaald' => ['Familie afgeh.:', 'family_collected', 'text', 'varchar:255'],
            ]
        ],
        [
            'label' => 'Dragers',
            'fields' => [
                'carrier_1' => ['Drager 1:', 'carrier_1', 'text', 'varchar:255'],
                'carrier_2' => ['Drager 2:', 'carrier_2', 'text', 'varchar:255'],
                'carrier_3' => ['Drager 3:', 'carrier_3', 'text', 'varchar:255'],
                'carrier_4' => ['Drager 4:', 'carrier_4', 'text', 'varchar:255'],
                'carrier_5' => ['Drager 5:', 'carrier_5', 'text', 'varchar:255'],
                'carrier_6' => ['Drager 6:', 'carrier_6', 'text', 'varchar:255'],
                'carrier_7' => ['Drager 7:', 'carrier_7', 'text', 'varchar:255'],
                'carrier_8' => ['Drager 8:', 'carrier_8', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => '',
            'fields' => [
                'kist_dalen' => ['Kist dalen:', 'descend_coffin', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'nieuwe_graven' => ['Nieuwe graven:', 'new_graves', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'nieuwe_graven_nr' => ['Nr:', 'new_graves_nr', 'text', 'varchar:255'],
                'oude_graven' => ['Oude graven:', 'old_graves', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'oude_graven_nr' => ['No:', 'old_graves_no', 'text', 'varchar:255'],
                'gebruiker' => ['Gebruiker:', 'user', 'text', 'varchar:255'],
                'grafdelven_door' => ['Grafdelven door:', 'grave_digging_by', 'text', 'varchar:255'],
                'steen_afhalen' => ['Steen afhalen:', 'stone_pickup', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'grafkelder' => ['Grafkelder:', 'family_vault', 'text', 'varchar:255'],
                'klokluiden' => ['Klokluiden:', 'bell_ringing', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'klokluiden_door' => ['Klokluiden door:', 'bell_ringing_by', 'text', 'varchar:255'],
                'rijdendebaar' => ['Rijdendebaar:', 'driving_bier', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => 'Bloemist',
            'fields' => [
                'bloemist' => ['Bloemist:', 'flowerist', 'text', 'varchar:255'],
                'bloemstuk1_nr' => ['Bloemstuk nr:', 'flower1_number', 'text', 'varchar:255'],
                'bloem1tekst' => ['Bloem tekst:', 'flower1_text', 'text', 'varchar:255'],
                'bloemstuk2_nr' => ['Bloemstuk nr:', 'flower2_number', 'text', 'varchar:255'],
                'bloem2tekst' => ['Bloem tekst:', 'flower2_text', 'text', 'varchar:255'],
            ]
        ],
        [
            'label' => '',
            'fields' => [
                'navisite_in' => ['Navisite in:', 'after_visitors', 'text', 'varchar:255'],
                'aantal_personen' => ['Aantal personen:', 'visitors_count', 'text', 'varchar:255'],
                'consumpties' => ['Consumpties:', 'consumptions', 'text', 'varchar:255']
            ]
        ],
    ];

    //AANGIFTE OVERLIJDEN ARRAYS
    public static $aangifteForm = [
        [
            'label' => 'Overledene',
            'fields' => [
                'akte_nr' => ['Akte nr.', 'certificate_number', 'text', '*:varchar:255'],
                'gemeente' => ['Gemeente.', 'town', 'text', '*:varchar:255'],
                'datum van overlijden' => ['Datum van overlijden.', 'date_of_death', 'date', ''],
                'uur overlijden' => ['Uur van overlijden.<br><i>24 uur-tijdaanduiding gebruiken</i>', 'hour_of_death', 'time', 'varchar:255'],
                'naam en voornaam' => ['Naam en voornaam.', 'firstname', 'text', 'varchar:255'],
                'geboortegemeente en datum' => ['Geboortegemeente en datum.', 'birthcity_and_date', 'text', 'varchar:255'],
                'woongemeente' => ['Woongemeente.', 'living_commune', 'text', 'varchar:255'],
                'adres aldaar' => ['Adres aldaar.', 'address', 'text', 'varchar:255'],
                'beroep' => ['Beroep.', 'profession', 'text', 'varchar:255'],
                'ongehuwd / gehuwd met' => ['Ongehuwd / gehuwd met.', 'unmarried_married_to', 'text', 'varchar:255'],
                'weduwnaar van/ weduwe van/ gescheiden van<br><i>(naam en voornaam)</i>' => ['Weduwnaar van/ weduwe van/ gescheiden van<br><i>(naam en voornaam)</i>.', 'unmarried_married_to', 'text', 'varchar:255'],
                'naam en voornaam vader' => ['Naam en voornaam vader.', 'name_father', 'text', 'varchar:255'],
                'overleden vader' => ['Overleden.', 'father_dead', 'radio', ['j' => 'Ja', 'n' => 'Nee']],
                'naam en voornaam moeder' => ['Naam en voornaam moeder.', 'name_mother', 'text', 'varchar:255'],
                'overleden moeder' => ['Overleden.', 'mother_dead', 'radio', ['j' => 'Ja', 'n' => 'Nee']]

            ]
        ],
        [
            'label' => 'Aangever',
            'fields' => [
                'naam' => ['Naam.', 'name_declarant', 'text', '*:varchar:255'],
                'voornamen' => ['Voornamen.', 'firstnames_declarant', 'text', 'varchar:255'],
                'geboortedatum en geboortegemeente' => ['Geboortedatum en geboortegemeente.', 'birthdate_birthcommune', 'text', 'varchar:255'],
                'woonplaats' => ['Woonplaats.', 'city', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => 'Begravenis',
            'fields' => [
                'datum begravenis' => ['De begravenis vindt plaats op:', 'funeral_date', 'date', ''],
                'tijd begravenis' => ['De begravenis begint om:', 'funeral_hour', 'time', ''],
                'locatie begravenis' => ['De begravenis vindt plaats te:', 'funeral_location', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => 'Crematie',
            'fields' => [
                'datum crematie' => ['De crematie vindt plaats op:', 'cremation_date', 'date', ''],
                'tijd crematie' => ['De crematie begint om:', 'cremation_hour', 'time', ''],
                'locatie crematie' => ['De crematie vindt plaats te:', 'cremation_location', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => 'Erfgenaam',
            'fields' => [
                'naam van meerderjarige erfgenaam' => ['Naam van meerderjarige erfgenaam', 'adult_heir_name', 'text', 'varchar:255'],
                'adres erfgenaam' => ['Adres', 'heir_address', 'text', 'varchar:255'],
                'woonplaats erfgenaam' => ['Woonplaats', 'heir_city', 'text', 'varchar:255'],
                'meerderjarige overledene kinderen' => ['Meerderjarige overledene kinderen', 'adult_heir_children', 'text', 'varchar:255'],
                'minderjarige overledene kinderen' => ['Minderjarige overledene kinderen', 'minor_heir_children', 'text', 'varchar:255']
            ]
        ]
    ];

    //AANVRAAG SNEEK ARRAYS
    public static $aanvraagSneekForm = [
        [
            'label' => '',
            'fields' => [
                'aanvraag_voor' => ['Aanvraag voor', 'ceremony_type', 'radio', ['memorial_service' => ' Rouwdienst', 'cremation_ceremony' => ' Crematieplechtigheid']],
                'te_verwachte_personen' => ['Aantal te verwachte personen', 'expected_indiviuals', 'text', 'varchar:255'],
                'datum_uitvaart' => ['Datum uitvaart', 'date_funeral', 'date', ''],
                'aanvang_uitvaart' => ['Aanvang uitvaart', 'start_funeral', 'time', ''],
                'extra_tijd' => ['Extra tijd (na overleg)<br><i>Standaard tijd is <b>2:30</b></i>', 'extra_time', 'time', '']

            ]
        ],
        [
            'label' => 'Persoonsgegevens overledene',
            'fields' => [
                'naam' => ['Naam', 'name_declarant', 'text', 'varchar:255'],
                'geslacht' => ['Geslacht', 'sex', 'radio', ['m' => ' M', 'v' => ' V']],
                'voornamen' => ['Voornamen', 'firstname_declarant', 'text', 'varchar:255'],
                'adres' => ['Adres', 'address_declarant', 'text', 'varchar:255'],
                'postcode' => ['Postcode', 'zipcode', 'text', 'varchar:255'],
                'plaats' => ['Plaats', 'city_declarant', 'text', 'varchar:255'],
                'geboorteplaats' => ['Geboorteplaats', 'city_of_birth', 'text', 'varchar:255'],
                'geboortedatum' => ['Geboortedatum', 'date_of_birth', 'text', 'varchar:255'],
                'leeftijd_in_jaren' => ['Leeftijd in jaren', 'age_declarant', 'text', 'varchar:255'],
                'overlijdensplaats' => ['Overlijdensplaats', 'city_of_death', 'text', 'varchar:255'],
                'overlijdensdatum' => ['Overlijdensdatum', 'date_of_death', 'date', ''],
                'burgerlijke_staat' => ['Burgerlijke staat', 'maritual_status', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => 'Bijzonderheden afscheidsdienst',
            'fields' => [
                'sprekers' => ['Sprekers', 'speakers', 'text', 'varchar:255'],
                'sprekers_aantal' => ['Sprekers aantal', 'speaker_amount', 'text', 'varchar:255'],
                'liturgische_attributen' => ['Wenst u liturgische attributen te gebruiken', 'liturgical_attributes', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'attributen' => ['Attributen', 'liturgical_attribute', 'checkbox', ['candles' => ' Kaarsen', 'cross' => ' Kruis', 'other' => ' Anders']],
                'baar_dalen' => ['Wenst u de baar te laten dalen?', 'descend_bier', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'familie_laatst_verlaten' => ['Wenst de familie als laatste de aula te verlaten?', 'family_leaves_last', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'bloem_wensen' => ['Wensen t.a.v. de bloemen', 'flowerwishes', 'radio', ['flowerfield' => ' Bloemenveld', 'take_home' => ' Mee naar huis', 'other' => ' Anders']],
                'andere_bloemwens' => ['Andere bloemwens', 'other_flower_wish', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => '<b>Wensen beeld en geluid</b> <i>informeer naar mogelijkheden</i>',
            'fields' => [
                'muziek' => ['Muziek:', '', 'p', ''],
                'muziek_1' => ['1.', 'music_1', 'text', 'varchar:255'],
                'muziek_2' => ['2.', 'music_2', 'text', 'varchar:255'],
                'muziek_3' => ['3.', 'music_3', 'text', 'varchar:255'],
                'muziek_4' => ['4.', 'music_4', 'text', 'varchar:255'],
                'muziek_5' => ['5.', 'music_5', 'text', 'varchar:255'],
                'beeldmateriaal_presenteren' => ['Wenst u beeldmateriaal te presenteren?', 'present_video', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'dvd_opname' => ['Wenst u een DVD-opname?', 'present_DVD', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'tekst_eigen_audio' => ["De mogelijkheid bestaat om eigen audio CD's af te spelen. Deze moeten uiterlijk 24 uur voor aanvang van de dienst aanwezig zijn. Voor diensten op maandag geldt dat de CD's vrijdag voor  12:00 uur aangeleverd moeten worden.", '', 'p', '']
            ]
        ],
        [
            'label' => 'Aanvullende wensen/informatie afscheidsdienst',
            'fields' => [
                'aanvullende_wensen_afscheidsdienst' => ['', 'additional_wishes_funeral', 'textarea', 'varchar:255'],
            ]
        ],
        [
            'label' => 'Aanvullende wensen/informatie beeld en geluid',
            'fields' => [
                'aanvullende_wensen_beeld_geluid' => ['', 'additional_wishes_media', 'textarea', 'varchar:255']
            ]
        ],
        [
            'label' => 'Asbestemming',
            'fields' => [
                'tekst' => ['Binnen één maand na de crematie (wettelijke bewaartermijn) ontvangt de opdrachtgever uitgebreide informatie inclusief een formulier waarop hij/zij de wensen met betrekking tot de asbestemming kenbaar kan maken.', '', 'p', ''],
                'as_afhandeling' => ['Asafhandeling:', 'ashes_completion', 'radio', ['crematory' => ' Asafhandeling rechtstreeks door crematorium met de opdrachtgever<br>', 'funeral_director' => ' Asafhandeling via de uitvaartverzorger']]
            ]
        ],
        [
            'label' => 'Consumpties',
            'fields' => [
                'tekst' => ['Zie voor de consumpties de uitgebreide cateringlijst van ons crematorium. (via
                    uw uitvaartverzorger verkrijgbaar) Vanuit deze cateringlijst kunt u een keuze
                    maken. Naast de hierop vermelde consumpties zijn op aanvraag ook andere
                    consumpties verkrijgbaar. U kunt uw keuze ook doorgeven via
                    www.crematoriumsneek.nl/catering. Gebruik de code die u heeft gekregen van uw
                    uitvaartverzorger om in te loggen. Omdat wij met dagverse producten werken is
                    het van belang dat u uiterlijk 48 uur vóór aanvang van de plechtigheid de
                    gewenste consumpties aan ons doorgeeft.', '', 'p', '']
            ]
        ],
        [
            'label' => 'Ter attentie van de opdrachtgever',
            'fields' => [
                'akkoord_met_voorwaarden' => ['Door dit vakje (handmatig) aan te kruisen geeft de opdrachtgever te kennen op de hoogte te zijn en akkoord te gaan met de onderstaande voorwaarden:', 'terms_agreed', 'checkbox', ['j' => '']],
                'voorwaarden' => ['
                <ol>
                    <li>De opdrachtgever verklaart in geval van een crematie dat in het stoffelijke overschot geen pacemaker en/of Radionucliden (Jodium therapie 1-125 –implantaat verwijderd) aanwezig is/zijn;
                    </li>
                    <li>
                        De opdrachtgever machtigt de uitvaartondernemer voor het regelen/uitvoeren van de uitvaartplechtigheid/koffiekamer/condoleanceruimte;
                    </li>
                    <li>
                        De opdrachtgever verklaart op de hoogte te zijn van de algemene voorwaarden en van de huisregels van Uitvaartcentrum &amp; Crematorium Sneek (deze staan tevens vermeld op de website) en de daaruit voortvloeiende consequenties;
                    </li>
                </ol>
                ', '', 'p', '']
            ]
        ],
        [
            'label' => 'Gegevens opdrachtgever',
            'fields' => [
                'naam_en_voorletters' => ['Naam en voorletters:', 'client', 'text', 'varchar:255'],
                'adres' => ['Adres:', 'client_address', 'text', 'varchar:255'],
                'postcode' => ['Postcode:', 'client_zipcode', 'text', 'varchar:255'],
                'plaats' => ['Plaats:', 'client_city', 'text', 'varchar:255'],
                'telefoon' => ['Telefoonnummer:', 'client_phone', 'text', 'varchar:255'],
                'relatie_tot_overledene' => ['Relatie tot overledene:', 'client_relationship_to_deceased', 'text', 'varchar:255'],
                'email' => ['E-mailadres:', 'client_email', 'text', 'varchar:255'],
                'datum_getekend' => ['Datum:', 'client_sign_date', 'date', '']
            ]
        ],
        [
            'label' => 'Gegevens uitvaartonderneming/-vereniging (factuuradres)',
            'fields' => [
                'naam' => ['Naam:', 'undertaker_name', 'text', 'varchar:255'],
                'adres' => ['Adres:', 'undertaker_address', 'text', 'varchar:255'],
                'postcode' => ['Postcode:', 'undertaker_zipcode', 'text', 'varchar:255'],
                'plaats' => ['Plaats:', 'undertaker_city', 'text', 'varchar:255'],
                'telefoon' => ['Telefoonnummer:', 'undertaker_phone', 'text', 'varchar:255'],
                'uitvaartleider' => ['Uitvaartleider:', 'funeral_director', 'text', 'varchar:255'],
                'email' => ['E-mailadres:', 'undertaker_email', 'text', 'varchar:255'],
                'datum_getekend' => ['Datum:', 'undertaker_sign_date', 'date', '']
            ]
        ],
        [
            'label' => '',
            'fields' => [
                'akkoord_door_opdrachtgever' => ['Akkoord door opdrachtgever:', 'signature_client', 'checkbox', ['j' => '']]
            ]
        ]
    ];


    //AANVRAAG CREMATIE ARRAYS
    public static $aanvraagCrematieForm = [
        [
            'label' => '',
            'fields' => [
                'datum' => ['Datum:', 'date_funeral', 'date', ''],
                'tijd' => ['Tijd:', 'funeral_time', 'time', ''],
                'crematienummer' => ['Crematienummer:', 'cremation_number', 'text', 'varchar:255'],
            ]
        ],
        [
            'label' => 'Persoonsgegevens overledene',
            'fields' => [
                'naam_en_voorvoegsels' => ['Naam en voorvoegsels:', 'name_declarant', 'text', 'varchar:255'],
                'voornamen' => ['Voornamen:', 'firstname_declarant', 'text', 'varchar:255'],
                'geboren_op' => ['Geboren op:', 'date_of_birth', 'text', 'varchar:255'],
                'geboorteplaats' => ['Plaats:', 'city_of_birth', 'text', 'varchar:255'],
                'overleden_op' => ['Overleden op:', 'date_of_death', 'date', ''],
                'overlijdensplaats' => ['Plaats:', 'city_of_death', 'text', 'varchar:255'],
                'adres' => ['Adres:', 'address_declarant', 'text', 'varchar:255'],
                'postcode' => ['Postcode:', 'zipcode', 'text', 'varchar:255'],
                'woonplaats' => ['Woonplaats:', 'city_declarant', 'text', 'varchar:255'],
                'burgerlijke_staat' => ['Burgerlijke staat:', 'maritual_status', 'text', 'varchar:255'],
                'kerkgenootschap' => ['Kerkgenootschap:', 'denomination', 'text', 'varchar:255'],
                'lidmaatschap_yarden' => ['Lidmaatschap Yarden:', 'member_yarden', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'yarden_lidnummer' => ['Lidnummer:', 'yarden_member_number', 'text', 'varchar:255'],
                'tekst' => ['Verklaard wordt dat in het stoffelijk overschot géén pacemaker aanwezig is en dat er geen voorwerpen van glas in de kist gelegd zijn.', '', 'p', '']
            ]
        ],
        [
            'label' => 'Aanvrager van de crematie',
            'fields' => [
                'naam_en_voorletters' => ['Naam en voorletters:', 'client', 'text', 'varchar:255'],
                'adres' => ['Adres:', 'client_address', 'text', 'varchar:255'],
                'postcode' => ['Postcode:', 'client_zipcode', 'text', 'varchar:255'],
                'woonplaats' => ['Woonplaats:', 'client_city', 'text', 'varchar:255'],
                'telefoon' => ['Telefoonnummer:', 'client_phone', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => 'Uitvaartondernemer (factuuradres)',
            'fields' => [
                'naam' => ['Naam:', 'undertaker_name', 'text', 'varchar:255'],
                'adres' => ['Adres:', 'undertaker_address', 'text', 'varchar:255'],
                'postcode' => ['Postcode:', 'undertaker_zipcode', 'text', 'varchar:255'],
                'woonplaats' => ['Woonplaats:', 'undertaker_city', 'text', 'varchar:255'],
                'telefoon' => ['Telefoonnummer:', 'undertaker_phone', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => 'Gegevens plechtigheid aula (Standaard 45 minuten)',
            'fields' => [
                'aantal_personen' => ['Hoeveel belangstellenden verwacht u?', 'expected_indiviuals', 'text', 'varchar:255'],
                'aula_extra_tijd' => ['Aula extra tijd', 'extra_time_auditorium', 'text', 'varchar:255'],
                'na_overleg_met_crematorium' => ['Na overleg met crematorium', 'auditorium_extra_time_after_consulation_with_crematorium', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'kist_aan_oog_onttrekken' => ['Kist aan oog onttrekken tijdens dienst', 'to_keep_from_eyes', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'zo_ja_wanneer' => ['zo ja, wanneer', 'when_to_keep_from_eyes', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'liturgische_attributen' => ['Liturgische attributen bij katafalk', 'liturgical_attributes_at_catafalque', 'checkbox', ['candles' => ' Kaarsen', 'cross' => ' Kruis', 'other' => ' Anders']],
                'sprekers' => ['Sprekers', 'speakers:', 'text', 'varchar:255'],
                'sprekers_aantal' => ['Sprekers aantal:', 'speaker_amount', 'text', 'varchar:255'],
                'muziekuitvoering' => ['Muziekuitvoering', 'music_type', 'checkbox', ['crematory' => ' Muziek crematorium', 'own_music' => ' Eigen muziek', 'live_music' => ' Live muziek']],
                'muziek_1' => ['a.', 'music_1', 'text', 'varchar:255'],
                'muziek_2' => ['b.', 'music_2', 'text', 'varchar:255'],
                'muziek_3' => ['c.', 'music_3', 'text', 'varchar:255'],
                'muziek_4' => ['d.', 'music_4', 'text', 'varchar:255'],
                'muziek_5' => ['e.', 'music_5', 'text', 'varchar:255'],
                'aantal_cassette_opnames' => ['Aantal Cassetteopnames', 'amount_cassettes', 'text', 'varchar:255'],
                'aantal_cd_opnames' => ['Aantal Cd-opnames', 'amount_cds', 'text', 'varchar:255'],
                'aantal_dvd_opnames' => ['Aantal DVD-opnames', 'amount_DVDs', 'text', 'varchar:255'],
            ]
        ],
        [
            'label' => 'Gegevens condoleanceruimte (Standaard 25 minuten)',
            'fields' => [
                'gebruik_condoleanceruimte' => ['Gerbuik condoleanceruimte', 'use_condolences_room', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'koffiekamer_extra_tijd' => ['Koffiekamer extra tijd', 'coffee_room_extra_time', 'time', ''],
                'koffiekamer_extra_tijd_na_overleg' => ['Na overleg met crematorium', 'coffee_room_extra_time_after_consulation_with_crematorium', 'radio', ['j' => ' Ja', 'n' => ' Nee']],
                'consumpties' => ['Consumpties', 'consumption_types', 'checkbox', [
                    'coffee_tea' => ' Koffie/thee',
                    'soda' => ' Frisdrank',
                    'wine' => ' Wijn',
                    'port' => ' Port',
                    'sherry' => ' Sherry',
                    'beer' => ' Bieren',
                    'cake' => ' Cake',
                    'sandwiches' => ' Broodjes assortiment',
                    ]],
                'aantal' => ['Aantal:', 'consumptions_amount', 'text', 'varchar:255'],
                'speciale_wensen' => ['Speciale wensen', 'consumption_wishes', 'text', 'varchar:255']
            ]
        ],
        [
            'label' => 'Wensen t.a.v. eventuele bloemen',
            'fields' => [
                'aanvullende_wensen_bloemen' => ['', 'additional_wishes_flowers', 'checkbox', ['to_family' => ' Retour familie', 'crematory_terrain' => ' Selectie op crematoriumterrein', 'other_nl' => ' Anders nl.']],
                'aankomst_familie_stoet' => ['Aankomst familie/nabestaanden per stoet:', 'arrival_family_procession', 'radio', ['j' => 'Ja', 'n' => 'Nee']],
            ]
        ],
        [
            'label' => 'Opmerkingen/speciale wensen',
            'fields' => [
                'opmerkingen' => ['', 'remarks', 'textarea', 'varchar:255']
            ]
        ],
        [
            'label' => 'Asbestemming',
            'fields' => [
                'asbestemming' => ['', 'ashes_destination_type', 'checkbox', [
                    'general_niche' => ' Plaatsing in de Algemene nis voor een half jaar.<br>Binnen een maand ontvangt u een informatiepakket met alle mogelijke asbestemmingen.<br>',
                    'other' => ' Anders, nl: tzt gehaald door familie of M. Heslinga'
                    ]],
            ]
        ],
        [
            'label' => '',
            'fields' => [
                'datum' => ['Datum', 'signature_crematory_date', 'date', ''],
                'akkoord' => ['Akkoord aanvrager', 'signature_crematory', 'checkbox', ['j' => '']]
            ]
        ]
    ];



    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$form = FormController::$aangifteForm;
    	return view('invoer', compact('form'));
    }

    public function getPost()
    {
    	$input = Input::all();
    	return $input;
    }

    public function getRequestSneek()
    {
        $form = FormController::$aanvraagSneekForm;
        return view('aanvraagSneek', compact('form'));
    }

    public function getRequestSneekId($id)
    {
        $form = FormController::$aanvraagSneekForm;
        $DBDataGeneral = DB::table('general')
            ->join('funeral', 'general.deceased_id', '=', 'funeral.deceased_id')
            ->join('ceremony_auditorium', 'general.deceased_id', '=', 'ceremony_auditorium.deceased_id')
            ->select('*')
            ->where('general.deceased_id', '=', $id)
            ->get();
        return view('aanvraagSneek', compact('form', 'DBDataGeneral'));
    }

    public function postRequestSneekId($id)
    {
        $form = FormController::$aanvraagSneekForm;
        $input = Input::all();

        $insertArray = [];
        foreach ($form as $field) {
            foreach ($field['fields'] as $row) {
                if (!empty($row[1]) && !empty($input[$row[1]])) {
                    $insertArray[$row[1]] = $input[$row[1]];
                }
            }
        }

        /*DB::table('general')
            ->where('deceased_id', $id)
            ->update(
                $insertArray
            );*/

        return $insertArray;
    }

    public static function getRequestSneekPost()
    {
    	$input = Input::all();
    	return $input;
    }

    public static function getAannameFormBegroting()
    {
        $form = FormController::$mainForm;
        $costsForm = FormController::$costsForm;
        $funeralForm = FormController::$funeralForm;
        return view('aanname_form_begroting', compact('form', 'costsForm', 'funeralForm'));
    }

    public function postAannameFormBegroting()
    {
        $form = FormController::$mainForm;
        $costsForm = FormController::$costsForm;
        $funeralForm = FormController::$funeralForm;

        $input = Input::all();

        $insertArray = [];
        foreach ($form as $field) {
            foreach ($field['fields'] as $row) {
                if (!empty($row[1]) && !empty($input[$row[1]])) {
                    $insertArray[$row[1]] = $input[$row[1]];
                }
            }
        }

        $insertArrayCosts = [];
        foreach ($costsForm as $field) {
            foreach ($field['fields'] as $row) {
                if (!empty($row[1])) {
                    $insertArrayCosts[$row[1]] = $input[$row[1]];
                }
            }
        }

        $insertArrayFuneral = [];
        foreach ($funeralForm as $field) {
            foreach ($field['fields'] as $row) {
                if (!empty($row[1])) {
                    $insertArrayFuneral[$row[1]] = $input[$row[1]];
                }
            }
        }

       /* var_dump($insertArray);
        var_dump($insertArrayCosts);
        var_dump($insertArrayFuneral);*/

        DB::table('general')->insert([
            $insertArray
        ]);

        DB::table('costs')->insert([
            $insertArrayCosts
        ]);

        DB::table('funeral')->insert([
            $insertArrayFuneral
        ]);

        DB::table('ceremony_auditorium')->insert(
            ['expected_indiviuals' => '']
        );

        return redirect('overzicht');
    }

    public static function getRequestCremation()
    {
        $form = FormController::$aanvraagCrematieForm;
        return view('aanvraagCrematie', compact('form'));
    }

    public static function postRequestCremation()
    {
        $input = Input::all();
        return $input;
    }
}
