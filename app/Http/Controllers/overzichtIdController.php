<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class overzichtIdController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index($id)
    {
		$row = DB::table('general')
			->where('deceased_id', $id)
			->first();
		return view('pdf/idPage', compact('row'));
    }
}
