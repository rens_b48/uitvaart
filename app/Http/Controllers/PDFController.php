<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use PDF;

class PDFController extends Controller
{
    public function getPDF($id){
    	$row = DB::table('general')
    		->join('funeral', 'general.deceased_id', '=', 'funeral.deceased_id')
			->where('general.deceased_id', $id)
			->first();
   
    	$pdf=PDF::loadView('pdf/aangiftePDF', ['row' => $row]);
    	return $pdf->stream('aangifte.pdf');
    }
    public function getPDF2($id){
    	$row = DB::table('general')
            ->join('funeral', 'general.deceased_id', '=', 'funeral.deceased_id')
            ->where('general.deceased_id', $id)
            ->first();
   
    	$pdf=PDF::loadView('pdf/aanvraagSneekPDF', ['row' => $row]);
    	return $pdf->stream('aanvraag_sneek.pdf');
    }
    public function getPDF3($id){
    	$row = DB::table('general')
            ->join('funeral', 'general.deceased_id', '=', 'funeral.deceased_id')
            ->where('general.deceased_id', $id)
            ->first();
   
    	$pdf=PDF::loadView('pdf/aanvraagGoutumPDF', ['row' => $row]);
    	return $pdf->stream('aanvraag_goutum.pdf');
    }
}
