<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Pagination\LengthAwarePaginator;

use App\Http\Requests;
use DB;

class overzichtController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    } 
    public function index()
    {
		$output = DB::table('general')
			->orderBy('deceased_id', 'desc')
			->paginate(15);
		return view('overview', compact('output'));
    }
    public function search_results($input)
    {
    	//var_dump($input); 
		
			$output = DB::table('general')
			->where('name_declarant', 'like', '%'. $input . '%')
			->orderBy('deceased_id', 'desc')
			->paginate(15);
		
		return view('overview', compact('output'));
    }   
}


